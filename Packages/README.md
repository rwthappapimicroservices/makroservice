# Packages

This folder and submodule is used for libraries and packages that we provide for the microservices.


## Create new Library:
1. Create new project in the subgroup [packages](https://git.rwth-aachen.de/rwthappapimicroservices/packages) and use the group template for packages.
2. Add the project here as submodule: `git submodule add <link_to_project.git>`
3. Rename `Template.csproj` and replace the `PACKAGENAME` in it.
4. Create a solution file: `dotnet new sln`
5. Add the `.csproj` to the solution: `dotnet sln add <projectname.csproj>`
6. Take a look at `Template.cs` and start adding your code in the package.
7. Describe and document the library in the readme :)


## Create new Library without template:
1. `dotnet new classlib -n <projectname>`
2. Add package metadata to your `.csproj` like [described here](https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli#add-package-metadata-to-the-project-file )
3. Add `<GeneratePackageOnBuild>true</GeneratePackageOnBuild>` to your `.csproj`
4. Now every time you run `dotnet pack` or `dotnet build` a `.nupkg` is created in `bin/Debug`. You need to upload this file.


## Upload Packages:
1. `dotnet restore`
2. `dotnet build`
3. `dotnet pack`
4. `dotnet nuget push bin/Debug/<packageid>.nupkg -k <apikey>`


## NuGet Setup
### Create NuGet Account and get a key:
1. Create a normal NuGet Account
2. Create a Microsoft Account
    - If it doesn't work because you use your work E-Mail Adress, try it with starting by one of these links (not sure with works):
        - login.microsoftonline.com
        - my.visualstudio.com
3. Connect your NuGet Account with the Microsoft Account
4. Create a key and save it somewhere (you can't copy it again later) like [described here](https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli#publish-the-package)

### Configure Nuget:
1. Add this to `~/.nuget/NuGet/NuGet.Config` into `<configuration>` tag:
    ```xml
    <config>
        <add key="defaultPushSource" value="https://api.nuget.org/v3/index.json" />
    </config>
    ```
2. Adding your API-Key to the config doesn't work because of [this issue](https://github.com/NuGet/Home/issues/6437)



## Links:
- https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli
