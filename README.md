## Setup
First you need some extra steps. There are two docker-compose files in this repository. One for the Microservices and one for GrayLog, an external Logging-System. To run the Microservices you first need to configure GrayLog.

1. Start GrayLog:
    - `docker-compose -f GrayLog/docker-compose.yml up -d`
2. Goto http://127.0.0.1:9000/system/inputs and add a new input
    - Add the global input `GELF HTTP` on Port 12201

After this, you can start developing.

## Developing
The script `./start.sh` starts the dependencies (currently only GrayLog) and then builds all Microservices. Alternatively you can manually start GrayLog with `docker-compose -f GrayLog/docker-compose.yml up -d` and then build and run the Microservices with `docker-compose up --build`.

## API
`curl localhost:5000/eLearning/Moodle/Semester`
`curl localhost:5001/AktuellesSemester`

## Add Microservices
1. Create a new git project
2. Put some code into it and push it to gitlab
3. Add this git project as submodule to `Microservices/`
4. Add the project to `docker-compose.yml`
5. Add a variable for the image tag to the files `env.compose.latest` and `env.compose.testing`

## Environment Files
If your project contains environment variables for configuration, you should add an example environment file to the projekt. That file should show all variables that are needed, maybe with some defaults. Remeber not to add secrets to this example, because the project is public.

Put the real environment file with all secret value into the `Environment/` submodule. This git project is private.

If you have different configurations for the project in our testing and live (latest) system, you should add two example files like it is shown in [Microservices/RWTHMoodle/Environment/](Microservices/RWTHMoodle/Environment/). Then add a variable for the image tag in the compose file
