# Vorbereitungen:
1. Docker in Windows (auf dem Server) installieren. Dabei habe ich die Docker Desktop Edge Version genommen, weil die andere nicht funktioniert hat.
2. Docker in Windows so einstellen, dass es Linux Container nutzt
    - Hier kann man jetzt in der Powershell testen ob Docker läuft.
3. Gitlab Runner in Windows installieren
4. Einen Gitlab Runner registrieren:
    - Dabei muss darauf geachtet werden, dass der Docker Socket als Volume eingebunden wird
    - https://tutorials.technology/tutorials/gitlab-ci-push-docker-images-to-registry.html
    - `gitlab-runner register -n --url https://git.rwth-aachen.de/ --registration-token <TOKEN> --executor docker --description "A gitlab runner using Docker socket bind" --docker-image "docker:stable" --docker-volumes /var/run/docker.sock:/var/run/docker.sock`
    - Zusatz Informationen:
        - Wenn man einen Linux Docker Container in Windows normal im Terminal starten will und den Docker Socket mounten will, dann muss man den Pfad `//var/run/docker.sock` (__vorne doppeltes `/`!__) nehmen. Beim Gitlab Runner jedoch, muss man den Pfad `/var/run/docker.sock` (__vorne einzelnes `/`!__) nehmen.
5. Ab und zu geht Docker in Windows kaputt und es kommt eine Fehlermeldung irgendwas mit `pipe`. Wenn das kommt, muss man nur einmal Docker switchen, dass es Windows Container nutzt und dann zurück switchen, dass es Linux Container nutzt

# Projekt Aufbau:
Jeder Microservice hat einen bestimmten Aufbau. Dabei sind diese Ordner und Dateien notwendig:
1. Der Hauptordner: `mkdir RWTHMoodle && cd RWTHMoodle`
2. Der Projektordner: `dotnet new web -o Moodle`
3. Der Unittestordner: `dotnet new xunit -o MoodleUnittest`
4. Dockerfiles: `touch Moodle/dockerfile MoodleUnittest/dockerfile`
5. CI-File: `touch .gitlab-ci.yml`
6. Eine Readme: `touch README.md`

7. Eine Solution-File erstellen:
    1. Erstelle eine Solution-File: `dotnet new sln`
    2. Füge alle `.csproj`s der Projekte hinzu: `dotnet sln add Moodle/Moodle.csproj && dotnet sln add MoodleUnittest/MoodleUnittest.csproj`

Jetzt sollte die Struktur in etwas so aussehen:

```
./RWTHMoodle/
./RWTHMoodle/.git/
./RWTHMoodle/.gitlab-ci.yml
./RWTHMoodle/RWTHMoodle.sln
./RWTHMoodle/Moodle/
./RWTHMoodle/Moodle/...
./RWTHMoodle/Moodle/Moodle.csproj
./RWTHMoodle/Moodle/dockerfile
./RWTHMoodle/MoodleUnittest/
./RWTHMoodle/MoodleUnittest/...
./RWTHMoodle/MoodleUnittest/MoodleUnittest.csproj
./RWTHMoodle/MoodleUnittest/dockerfile
```

Danach müssen die `.gitlab-ci.yml` und die beiden Dockerfiles Konfiguriert werden.

## CI-CD:
Die Datei `.gitlab-ci.yml` enthält die Konfiguration der CI-Pipeline. Dabei wird [diese Vorlage](example.gitlab-ci.yml) empfohlen, in welcher `<ProjectFolder>` und `<UnittestFolder>` ersetzt werden muss.

## Dockerfiles:
Als Vorlage der Dockerfile im Projekt dient [dieses Beispiel](example.dockerfile). Dort muss in der letzten Zeile `<ProjectName>` ersetzt werden.
Als Vorlage der Dockerfile im Unittest-Projekt dient [das andere Beispiel](example.dockerfile.unittest).

## README:
In der Readme stehen allgemeine und wichtige Information zu dem Microservice, darunter eine Beschreibung, eine **Auflistung aller benötigten Environment-Variablen** und der Abhängigkeiten zu anderen Microservices. Das ist notwendig, damit eine reibungslose einfache Integration dieses Services möglich ist.

## Einbinden als Git-Submodule:
Zum derzeitigen Zeitpunkt ist das neue Projekt ein Unterordner dieses Projektes. Allerdings wollen wir die Unterprojekte als Submodules einbinden. Dazu sind folgende Schritte notwendig:
1. Projekt in Gitlab erstellen. Für den neuen Microservice soll in dem RWTH-Gitlab in der Gruppe ein Projekt angelegt werden.
2. Projekt in das Repo pushen.
3. Kontrollieren ob es wirklich im Repo im Gitlab ist um Datenverluste zu vermeiden.
4. Den Ordner mit dem Projekt lokal löschen.
5. Das erstellte Projekt von Gitlab als Submodule einbinden.

## Fertig:
Jetzt kann der neue Microservice programmiert werden :)
