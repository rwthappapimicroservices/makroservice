## Migration Notices:
- Our RestClient is now a NuGet-Package and should be added with `dotnet add package PIT.Labs.RestClient`
    - This now uses a new namespace: `using PIT.Labs.RestClientLib`

