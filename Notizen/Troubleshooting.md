
## Updating a dependency
If updating a dependency goes wrong, try clearing the cache: `dotnet nuget locals all -c`


