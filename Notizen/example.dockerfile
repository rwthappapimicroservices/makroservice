FROM mcr.microsoft.com/dotnet/core/sdk:3.0 as build
RUN apt update
RUN apt upgrade -y
RUN apt install mono-devel -y
RUN mozroots --import --sync
COPY . /app
WORKDIR /app/
RUN dotnet publish -c Release

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0
COPY --from=build /app/bin/Release/netcoreapp3.0/publish /app/
WORKDIR /app
CMD ["dotnet", "/app/<ProjectName>.dll"]
